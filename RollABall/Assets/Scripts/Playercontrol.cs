﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Playercontrol : MonoBehaviour { 

    private Rigidbody rb;
    public float speed;
    private int count;
    public Text CountText;
    public Text WinText;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        WinText.text = "";
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float jump = Input.GetAxis("Jump");
        
        Vector3 movement = new Vector3 (moveHorizontal, jump, moveVertical);

        rb.AddForce(movement * speed);

    }

    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            speed = speed + 1;
        }
        if (Input.GetKeyDown("2"))
        {
            speed = speed - 1;
        }
        if (Input.GetKeyDown("space"))
        {
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
     if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }
    void SetCountText()
    {
        CountText.text = "Count: " + count.ToString();
        if (count>= 27)
        {
            WinText.text = "You Win!";
        }
    }
}
